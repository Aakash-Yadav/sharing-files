# Sharing Files 

Quickly Sharing Files Over a Local Network with Python


## Quickly Sharing Files Over a Local Network with Python

For anyone who has had to work on multiple computers, or work on a project with others, they have inevitably had to share files between machines. Luckily, in today’s age with cloud services like Google Drive or Dropbox, one can easily share files with anyone. However, it can still become tedious uploading and downloading new versions of a file from one of these services, or even a flash drive.

Enter Python’s http.server module which lets you quickly deploy a local webserver in any directory, which when navigated to on a web browser will contain all files in said directory. While the server is running, any machine on the wi-fi network will be able to download the files. That being said, this is not a secure form of file transfer, you should avoid hosting any sensitive data, or using this method over a public network.

### Step 1 — Start the Server

Navigate to the directory (folder) which you wish to share and launch a new terminal window (Mac/Linux) or command prompt (windows) and type the following command:

> $ python -m http.server
> ## $ py -m http.server ## for windows users


If successful, you should see Serving HTTP on :: port 8000 (http://[::]:8000/) … pop up. You are now hosting the files to a local web

### Step 2 — Get your IP Address

There are a few ways to get this info, but the quickest would be from the command line. For Mac users, in a new terminal window type the following to get your machines IP address:

> $ ifconfig 

### step 3 - open python File 

![alt text](https://xp.io/storage/1PsBcuDp.png)

It will grab data from Host Ip 

### Step 4 Download The  Fiel You want:

![alt text](https://xp.io/storage/1PsJuImQ.png)
now i select index 0 for image 

### Out_Put:
![alt text](https://xp.io/storage/1PsNfojn.png)






