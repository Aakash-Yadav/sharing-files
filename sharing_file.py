'import'
import sys
from bs4 import BeautifulSoup
from requests import Session
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor
import urllib.parse
from ifcfg import interfaces
from os import system, mkdir

FEATCH = Session().get


def retrun_local_ip():
    session_local_api = ""
    for name, interface in interfaces().items():
        if (interface['inet4'][0][0:4] == "192."):
            session_local_api += (interface['inet4'][0])
            break
    return session_local_api


try:
    Self_local_api = retrun_local_ip()
    print("YOUR LOCAL PI = ", Self_local_api)
except:
    print("ConnectionError /(Connect to wifi)")
# print( retrun_local_ip())


def thread_pool_executor(function, value):
    with ThreadPoolExecutor() as exe:
        data = exe.map(function, value)
    return data


class get_name_of_file_from_local_host(object):
    def __init__(self):  # 192.168.1.106
        self.url_of_local_host = self.get_live_active()[-1]
        print(self.url_of_local_host)
        self.soup = BeautifulSoup(
            FEATCH(self.url_of_local_host).text, 'html.parser')
        self.file_name = ''
        self.list_name = []

    def empty_server(self, api):
        if not api:
            raise ConnectionRefusedError("No python server found at port 8000")
        elif len(api) > 1:
            for i, j in enumerate(api):
                print(f"Select server [{i}],{j}")
            index_value_by_user = int(
                input('Enter [ ] num To select server >> '))
            try:
                api_key = [api[index_value_by_user]]
            except Exception:
                return self.empty_server(api)
            return api_key
        else:
            return api

    def scraping_a_tag(self):
        for a_tag in self.soup.find_all('a'):
            if (a_tag['href'][-1] == "/"):
                continue
            else:
                self.list_name.append(a_tag['href'])
        self.list_name = sorted(self.list_name)
        return sorted(self.list_name)

    def get_live_active(self):
        api = [];
        for i in range(10):
            try:
                LINK = f'http://{Self_local_api[:-1]}{i}:8000/'
                if (FEATCH(LINK).status_code == 200):
                    print("Sucess")
                    if (not (Self_local_api == LINK[7:-6])):
                        api.append(LINK)
                        break;
            except:
                print(f"ConnectionError for {LINK[7:-6]}")
        return api

    def print_all_file_name(self):
        system("clear")
        for i, filename in enumerate(self.scraping_a_tag()):
            print('[%d] %s ' % (i, urllib.parse.unquote(filename)))
        print('')
        index_value_by_user = int(input('Enter [ ] num To download file >> '))
        self.file_name += self.list_name[index_value_by_user]
        return self.url_of_local_host+self.list_name[index_value_by_user]


class Download_file_using_bs4(get_name_of_file_from_local_host):
    def main_dwnload(self):
        try:
            mkdir("Download_File")
        except:
            print("Folder exist")
        self.data_of_file = FEATCH(self.print_all_file_name(), stream=True)
        self.chunk = 1024
        self.total_size = int(self.data_of_file.headers['content-length'])
        with open(f"Download_File/"+urllib.parse.unquote(self.file_name), 'wb') as file:
            for data in tqdm(self.data_of_file.iter_content(chunk_size=self.chunk), total=self.total_size/self.chunk, unit='KB'):
                file.write(data)
        return True


if __name__ == '__main__':
    print(Download_file_using_bs4().main_dwnload())